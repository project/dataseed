Dataseed Search API
---------------

In order for this module to work, you will first need a valid Dataseed account.

Once you got your Dataseed account:

  - Go to the dataseed module config page (admin/config/system/dataseed) and enter your
  Dataseed credentials

  - Create a new Search API server (admin/config/search/search_api/add_server)
  choosing as service class "DataseedService"

  - Create an index (admin/config/search/search_api/add_index) selecting as
  server the one created at the previous point
    - select the fields to be indexed
    - Reindex
